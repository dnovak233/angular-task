export function monthNameShort(date: string) {
  return new Date(date).toLocaleString('default', { month: 'short' })
}
