export interface IChartData {
  date: string;
  value: number;
  color?: string;
}
