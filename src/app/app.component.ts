import {Component, OnInit} from '@angular/core';
import {IChartData} from './common/models/ChartData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data: IChartData[] = [];
  reportedData?: IChartData | null;

  ngOnInit() {
    this.setData();
  }

  reportData(d: IChartData) {
    console.log(d);
    this.reportedData = d;
  }

  setData() {
    this.data = [
      {date: '2018-01-01', value: 270770.647, color: 'green'},
      {date: '2018-02-01', value: 273220.320 },
      {date: '2018-03-01', value: 253580.124},
      {date: '2018-04-01', value: 254728.845},
      {date: '2018-05-01', value: 244422.258},
      {date: '2018-06-01', value: 244422.258},
      {date: '2018-07-01', value: 244422.258},
      {date: '2018-08-01', value: 244422.258, color: 'blue'},
      {date: '2018-09-01', value: 244422.258},
      {date: '2018-10-01', value: 244422.258},
      {date: '2018-11-01', value: 244422.258},
      {date: '2018-12-01', value: 244422.258, color: 'yellow' }
    ];
  }

  clearData() {
    this.data = [];
    this.reportedData = null;
  }
}
