import {Component, Input} from '@angular/core';
import {IChartData} from '../../common/models/ChartData';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent {
  @Input() data?: IChartData | null;
}
