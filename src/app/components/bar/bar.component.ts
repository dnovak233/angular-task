import {Component, Input, OnChanges, Output, EventEmitter, SimpleChanges} from '@angular/core';
import * as d3 from 'd3';
import {IChartData} from '../../common/models/ChartData';
import {monthNameShort} from '../../common/utils/date-fromat';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent implements OnChanges {
  @Input() data: IChartData[] = [];
  @Input() defaultColor = '#FF5E5E';
  @Output() onBarClick = new EventEmitter<IChartData>();

  private svg: any;
  private margin = 50;
  private width = 750 - (this.margin * 2);
  private height = 400 - (this.margin * 2);

  private deleteSvg(): void {
    d3.select('figure#bar>svg').remove()
  }

  private createSvg(): void {
    this.svg = d3.select('figure#bar')
      .insert('svg')
      .attr('width', this.width + (this.margin * 2))
      .attr('height', this.height + (this.margin * 2))
      .append('g')
      .attr('transform', `translate(${this.margin}, ${this.margin})`);
  }

  private drawBars(data: IChartData[]): void {
    // Create the X-axis band scale
    const xScale = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map((d: IChartData) => monthNameShort(d.date)))
      .padding(0.2)

    // Draw the X-axis on the DOM
    this.svg.append('g')
      .attr('transform', `translate(0, ${this.height})`)
      .call(d3.axisBottom(xScale))
      .selectAll('text')
      .style('text-anchor', 'center');

    // Create the Y-axis band scale
    const yScale = d3.scaleLinear()
      .domain([0, d3.max(data.map(({ value }: IChartData) => value)) || 0])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append('g')
      .call(d3.axisLeft(yScale));

    // Create and fill the bars
    this.svg.selectAll('bars')
      .data(data)
      .enter()
      .append('rect')
      .on('click', (e: Event, d: IChartData) => { this.onBarClick.emit(d) })
      .attr('x', (d: IChartData) => xScale(monthNameShort(d.date)))
      .attr('y', yScale(0))
      .attr('height', this.height - yScale(0))
      .attr('width', xScale.bandwidth())
      .attr('fill', (d: IChartData) => d.color || this.defaultColor)

    this.svg.selectAll('rect')
      .transition()
      .duration(800)
      .attr('y', (d: IChartData) => yScale(d.value))
      .attr('height', (d: IChartData) => this.height - yScale(d.value))
      .delay((d: IChartData, i: number) => i*100)
  }

  private updateGraph() {
    if (this.svg) {
      this.deleteSvg();
    }

    this.createSvg();
    this.drawBars(this.data);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.updateGraph();
  }

}
